#!/bin/bash

set -euo pipefail
#set -euxo pipefail
#
# CREATOR
# This code in writen by nanorocks
#

#
# POINT
# Helping script to organize your files in folders
#

#
# USAGE
# For OS's who compile SHELL SCRIPTS
#

#GLOBAL VARIABLES
full_path=0
tmp_path=0

function menu_print() # printing menu with echo 1,2,3,4 /DONE
{
echo -e "\n  MENU                                                 "
echo "  1) USAGE                                                  "
echo "  2) Organize specific type BY: < .NameExtension >          "
echo "  3) Find specific type BY: < .NameExtension >              "
echo "  4) EXIT                                                   "
echo "                                                            "
}

function logo() # printing logo with echo and important things /DONE
{
echo "+++++++++++++++++++++++++++++++++++++++++++++"
echo "+  _       _                     _          +"
echo "+ |_o| _  / \.__  _.._ o_  _ ._ |_ |_  _ || +"
echo "+ | ||(/_ \_/|(_|(_|| ||/_(/_|   _|| |(/_|| +"
echo "+              _|                           +"
echo "+++++++++++++++++++++++++++++++++++++++++++++"

echo -e "\nIMPORTANT: Before using this shell script, read the 1) USAGE section of the menu below." 

}

function rec() { # recursion starting on begining to rename files with spaces in their title  /DONE
 rename 's/ /_/g' *
 for i in `ls $tmp_path`
 do
    #echo $i, `file $i`
    if [ -d $i ] 
    then 
        # echo "Directorium: $i"
        cd $i
        rec .
        cd ..
    fi
 done
}

function validation_start() # important thing for PATH validation /DONE
{
echo -e "-> Enter your path to folder: "
read path

# if ! [ ${path:0:1} = "/" ]  
# then
    # slas="/"
    # path=$slas$path
    # echo "You path must start with '/home'"
# fi

if ! [ ${path:0:5} = "/home" ] # only works on /home 
then
    echo -e "You path must start with '/home'"
    exit 0
fi

while ! [[ -d "$path" ]];   # validation if path is valid dir
do 
    echo -e "-> Invalid path! Try again or ctrl+c/ctrl+z to EXIT."
    echo -e "-> Enter your path to folder: "
    read path
    # exit 0
done

echo -e "-> Valid path!"
cd $path
tmp_path=$path
rec .
}

function menu_selection_validation() # validation on menu -> numeric valid number or text /DONE
{
menu_print .
echo "-> Select number: "
read num

re='^[0-9]+$'

while ! [[ $num =~ $re ]] ; do #fix bugs here DONE
   echo "-> Not a number. Enter valid number.";
   read num
done

while [[ $num -gt 4 ]] ; do  #fix bugs here DONE
   echo "-> Not a valid number. Try again.";
   read num
done

}

function validation_yes_no() # validation for yes/no answers /DONE
{

read three_tmp
        
while [ "$three_tmp" != "y" ] && [ "$three_tmp" != "n" ]; 
do
    echo "Invalid input."
    echo -e "$*" 
    read three_tmp
done
}

function main()
{

if [ $num = "4" ] # terminating shell script /DONE
then    
    echo "-> Terminated. Bye. "
    exit 0
fi

if [ $num = "3" ] # find specific type BY NameExtension /DONE
then 
    if [ $full_path = 0 ] 
    then 
        full_path=1
        validation_start .
    fi
    
    echo -e "\n-> Enter file type: "
    read format_files
    
    if [[ $format_files != "."* ]]; then # validation format --> /DONE
        format_files="."$format_files
    fi
    
    tmp=`find $path -name "*$format_files"`
    size=${#tmp}

    str="-> Do you want to find NEW specific type (y/n)?"
    
    while [[ "$size" = 0 ]] 
    do 
        echo "-> File type not exist in: "`pwd`
        echo "$str" 
         
        validation_yes_no $str # validation format if NOT begins with "." add one e.g mp3 -> .mp3 |--> /DONE
        
        if [ $three_tmp = "y" ] 
        then 
            main .
            exit 0
        elif [ $three_tmp = "n" ] 
        then 
            echo "-> Redirect to MENU."
            sleep 0.5
            menu_selection_validation .
            main .
            exit 0
        fi
    done
    
    echo -e "$tmp\n"
    echo "$str"
    
    validation_yes_no $str  # add here validation --> DONE
    
    if [ $three_tmp = "y" ] 
    then 
        main .
        exit 0
    elif [ $three_tmp = "n" ] 
    then 
        echo "-> Redirect to MENU."
        sleep 0.5
        menu_selection_validation .
        main .
        exit 0
    fi
    
fi

if [ $num = "2" ] # organize specific type BY NameExtension /DONE
then 
    if [ $full_path = 0 ] 
    then 
        full_path=1
        validation_start .
    fi
    echo -e "\n-> Enter file type: " # add here validation
    read format_files
    
    if [[ $format_files != "."* ]]; then # validation format if NOT begins with "." add one e.g mp3 -> .mp3 |--> /DONE
        format_files="."$format_files
    fi
    
    tmp=`find $path -name "*$format_files"`
    size=${#tmp}
    
    while [[ "$size" = 0 ]] 
    do 
        echo "-> File type not exist in: "`pwd`
        str="-> Do you want to organize NEW specific type (y/n)?"
        echo "$str" # add here validation ---> DONE
        
        validation_yes_no $str
       
        if [ $three_tmp = "y" ] 
        then 
            main .
            exit 0
        elif [ $three_tmp = "n" ] 
        then 
            echo "-> Redirect to MENU."
            sleep 1
            menu_selection_validation .
            main .
            exit 0
        fi
    done
    
    if [ "$size" -gt 0 ] 
    then
        echo -e "\n-> Enter folder name:" # add here validation
        read fname
        
        while [ -d $path$fname ] 
        do 
            echo "-> Folder exist. Enter NEW folder name:"
            read fname
        done
        
        mkdir $fname
        
        for i in $tmp 
        do 
            echo "-> MOVING: $i to $fname"
            mv $i $fname
            sleep 0.2 
        done
    fi
    
    str=" \n-> Do you want to organize NEW specific type (y/n)?"
    echo -e "$str" # add here validation ---> DONE
    
    validation_yes_no $str 
    
    if [ $three_tmp = "y" ] 
    then 
        main .
        exit 0
    elif [ $three_tmp = "n" ] 
    then 
        echo "-> Redirecting to MENU."
        sleep 0.5
        menu_selection_validation .
        main .
        exit 0
    fi
fi

if [ $num = "1" ] # usage for this shell script IMPORTANT andn RECOMENDATION things
then 
    echo -e "\n--->IMPORTANT<--- \n\nTHIS SHELL SCRIPT WON'T WORK PROPERLY IF:"
    echo "-> Your files/folders have blank spaces in their name."
    echo "-> Your existing folders in your inserted path begin with special characters like (?/'][{\!@#...)."
    echo "-> The name of the folder that you created contains special characters at the beginning like (?/'][{\!@#...)."
    echo "-> If you repeatedly enter invalid values."
    echo "-> If you do not have good knowledge of scripts, do not modify the code."
    
    echo -e "\nRECOMENDATION:"
    echo "-> If you do not satisfy any of the requirements above, this shell script will not work well. So arrange them before turning it on to work."
    echo "-> If you need to use special characters when creating a folder, it is desirable to use - and _ for your folder name."
    echo -e "-> If you get stuck somewhere, contact me on: andrejnankov@gmail.com \n"
    
    str="Do you want to be redirect to MENU (y/n)?"
    echo "$str" 
    
    validation_yes_no $str  # add here validation ---> DONE
        
    if [ $three_tmp = "y" ] 
    then 
        echo "-> Redirecting ..."
        logo .
        sleep 0.5
        menu_selection_validation .
        main .
        exit 0
        
    elif [ $three_tmp = "n" ] 
    then 
        echo "-> Terminated. Bye. "
        exit 0
    fi
    
    exit 0
fi
}

logo .
menu_selection_validation .
main .
